import torch
import numpy as np


def y_h(x):
    Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[i] for i in range(8)]
    Numer = 2*np.pi*Tu
    return (Numer*(Hu-Hl))/(np.log(r/rw)*(1+((2*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+(Tu/Tl)))
def y_l1(x):
    Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[i] for i in range(8)]
    Numer = 2*np.pi*Tu
    return (Numer*(Hu-.8*Hl))/(np.log(r/rw)*(1+((1*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+(Tu/Tl)))
def y_l2(x):
    Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[i] for i in range(8)]
    Numer = 2*np.pi*Tu
    return (Numer*(Hu+3*Hl))/(np.log(r/rw)*(1+((8*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+.75*(Tu/Tl)))
def y_l3(x):
    Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[i] for i in range(8)]
    Numer = 2*np.pi*Tu
    return (Numer*(1.09*Hu-Hl))/(np.log(4*r/rw)*(1+((3*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+(Tu/Tl)))

def y_l4(x):
    Tu, Hu, Hl, r, rw, Tl, L, Kw = [x[i] for i in range(8)]
    Numer = 2*np.pi*Tu
    return (Numer*(1.05*Hu-Hl))/(np.log(2*r/rw)*(1+((3*L*Tu)/(np.log(r/rw)*(rw**2)*Kw))+(Tu/Tl)))





def multi_fidelity_borehole(x):
    if type(x) == torch.Tensor:
        input_copy = x.clone()
    elif type(x) == np.ndarray:
        # np.ndarray.flatten(input)
        input_copy = np.copy(x)
    y_list = []
    for X in input_copy:
        if X[-1].numpy() == 1.0:
            y_list.append(y_h(X))
        elif X[-1].numpy() == 0.75:
            y_list.append(y_l3(X))
        elif X[-1].numpy() == 0.5:
            y_list.append(y_l4(X))
        elif X[-1] == 3.0:
            y_list.append(y_l3(X))
        elif X[-1]==4.0:
            y_list.append(y_l4(X))
        else:
            raise ValueError('Wrong label, should be h, l1, l2 or l3')
    return -1*torch.tensor(np.hstack(y_list))